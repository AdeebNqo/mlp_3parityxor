/*

@author Zola 'adeebnqo' Mahlaza
@version 2.0
@license see LICENCE

This class represents a multilayer perceptron network for
solving the 3 parity xor problem.
*/
#ifndef _network
#define _network
#include<vector>
namespace mhlzol004{
	class network{
		public:
			network(int input=3, int hidden=3, int output=1);			
			std::vector< std::vector<float> > network_grid;

	};
}
#endif
