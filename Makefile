app = lab3
com = g++ #compiler
header = neuron.hpp network.hpp
src = main.cpp neuron.cpp network.cpp
obj = neuron.o network.o #object files
options = -g -Wall
$(app): $(header) $(src) $(obj)
	@$(com) $(options) $(obj) main.cpp -o $(app)
neuron.o: $(header) network.o neuron.hpp
	@$(com) -c neuron.cpp
network.o: $(header) network.cpp
	@$(com) -c network.cpp
clean:
	@rm -f *.o $(app)
