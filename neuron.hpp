/*

@author Zola 'adeebnqo' Mahlaza
@version 1.0
@license see LICENCE

*/
#ifndef _neuron
#define _neuron
#include<vector>
namespace mhlzol004{
	struct connection{
		float weight;
		float input;
	};
	class neuron{
		public:
			float input,output;
			neuron();

			//neurons connected to
			std::vector<neuron> end_neurons;
			//weights for the links
			std::vector<connection> links;
	};
}
#endif
