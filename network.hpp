/*

@author Zola 'adeebnqo' Mahlaza
@version 1.0

*/
#ifndef _net
#define _net
#include "neuron.hpp"
namespace mhlzol004{
	class network{
		public:
			network();
			float dot_product(neuron input, neuron output);
		private:
			std::vector<neuron> input;
			std::vector<neuron> hidden;
			std::vector<neuron> output;
	};
}
#endif
