/*
 
This file is subject to the terms and conditions defined in
file 'LICENSE', which is part of this source code package.

@author Zola 'adeebnqo' Mahlaza
@version 3.0
@date 20 August 2013

@qoute
	”My therapist told me the way to achieve true inner peace is to finish what I start. So far I’ve finished two bags of M&Ms and a chocolate cake. I feel better already.”
	-Dave Barry
	
*/
#include<iostream>
#include "neuron.hpp"
#include<math.h>
namespace mhlzol004{
	neuron::neuron(){
	};
	neuron::~neuron(){
		int num_outgoing = outgoing.size();
		for (int i=0; i<num_outgoing; ++i){
			delete outgoing[i];
		}
	}
	float neuron::sum(std::vector<float> inputs, std::vector<float> weights){
		typedef std::vector<float>::iterator _it;
		float current_sum = 0;
		_it inputs_end = inputs.end();
		
		//calculating the dot product of the two vectors: weights and inputs
		_it wit = weights.begin();
		for (_it iit = inputs.begin(); iit<inputs_end; ++iit, ++wit){
			current_sum += (*wit)*(*iit); 
		}
		return current_sum;
	};
	float neuron::sigmoid(float value){
		float neg_value = -1*value;
		float denominator = exp(neg_value)+1;
		return  1/denominator;
	}
}
