/*

This file is subject to the terms and conditions defined in
file 'LICENSE', which is part of this source code package.

@author Zola 'adeebnqo' Mahlaza
@version 3.0
@date 20 August 2013

@quote
	"I went to Zimbabwe. I know how white people feel in America now; relaxed! Cause when I heard the police car I knew they weren't coming after me!"
	-Richard Pryor

This class is the multilayer perceptron network.

*/
#ifndef _network
#define _network
#include<vector>
#include "neuron.hpp"
#include<math.h>
namespace mhlzol004{
	class network{
		typedef std::vector<neuron*> _lvector; 
		public:	
			/*
			#Constructor

			input - number of neurons in input layer
			hidden - number of neurons in hidden layer
			output - number of neurons in output layer
			*/
			network(int input=3, int hidden=2, int output=1);
			~network();
			_lvector input_layer;
			_lvector hidden_layer;
			_lvector output_layer;
			float learning_rate;
			void set_learning_rate(float rate);
			//method accessible from outside network for teaching networking
			void learn(std::vector<float> values, float output);
			//method for applying the 2 parity xor function
			float xor3(std::vector<float> inputs);

		private:
			//method for teaching the network a specific
			void teach(int num_inputs=3, std::vector<float> inputs=0);
			void really_teach(int num_inputs, std::vector<neuron*>& neurons);
			//backpropagation method
			void backpropagate(float expected_output);
	};
}
#endif
