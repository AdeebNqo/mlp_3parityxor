/*

This file is subject to the terms and conditions defined in
file 'LICENSE', which is part of this source code package.
 
@author Zola 'adeebnqo' Mahlaza
@version 3.0
@date 20 August 2013

@qoute 
	"When I die, I want to go peacefully like my grandfather did, in his sleep. Not screaming and yelling like the passengers in his car.”
	-Anonymous
*/
#ifndef _neuron
#define _neuron
#include<vector>
#include "util.hpp"
namespace mhlzol004{
	class neuron{
		public:
			neuron();
			~neuron();
			//sum function
			float sum(std::vector<float> inputs, std::vector<float> weights);
			//sigmoid function
			float sigmoid(float value);

			std::vector<edge*> incoming;
			std::vector<edge*> outgoing;
			
			//output of neuron
			float output;
			//error value
			float error;
	};
}
#endif
