Zola Mahlaza
lab3


Traning data:

Please see "test.txt"
Format of values in the file is as follows:

input0,input1,input2,output

Q1:

Minimum is two.

Q2:

The MLP didn't learn the 3 parity problem. It's outputs are incorrect, however, they are consistent. That is, it produces
the same specific values for each expected value. An example run reveals:

	#using two hidden neurons

expected:0, result: 0.454437
expected:1, result: 0.453305
expected:1, result: 0.453305
expected:0, result: 0.452174
expected:1, result: 0.453305
expected:0, result: 0.452174
expected:0, result: 0.452174

	#using three hidden neurons

expected:0, result: 0.443657
expected:1, result: 0.442261
expected:1, result: 0.442261
expected:0, result: 0.440867
expected:1, result: 0.442261
expected:0, result: 0.440867
expected:0, result: 0.440867

	(Using 99 iterations)


Compilation and more
====================

Please make sure the file "test.txt" is present. The values can be changed, however, they should still follow the same
format.

To compile, `make`.
To run, `make run`
To clean binaries, `make clean`
