/*

This file is subject to the terms and conditions defined in
file 'LICENSE', which is part of this source code package.

@author Zola 'adeebnqo' Mahlaza
@version 3.0
@date 20 August 2013

@quote
	"I'm now making a Jewish porno film. 10% sex, 90% guilt."
	-Henny Youngman
*/
#include "network.hpp"
#include<algorithm>
#include<iostream>
#include<vector>
namespace mhlzol004{

	network::network(int input, int hidden, int output){
		learning_rate = 0.05;
		//creating or populating input neurons
		for (int i=0; i<input; ++i){
			neuron* input_neuron = new neuron();
			input_layer.push_back(input_neuron);
		}
		//creating or populating hidden neurons
		for (int i=0; i<hidden; ++i){
			neuron* hidden_neuron = new neuron();
			hidden_layer.push_back(hidden_neuron);
		}
		//creating or populating output neurons
		for (int i=0; i<output; ++i){
			neuron* output_neuron = new neuron();
			output_layer.push_back(output_neuron);
		}
		/*
		
		Initialising the edge and weights between neurons

		*/
		
		//input neurons
		for (int i=0; i<input; ++i){
			neuron* tmpinput_neuron = input_layer[i];
			//connecting input neuron to all hidden neurons
			for (int j=0; j<hidden; ++j){
				neuron* tmphidden_neuron = hidden_layer[j];
				edge* tmp_edge = new edge(); tmp_edge->source = tmpinput_neuron; tmp_edge->dest = tmphidden_neuron;
				tmp_edge->weight = 0.05;
				tmpinput_neuron->outgoing.push_back(tmp_edge);
				tmphidden_neuron->incoming.push_back(tmp_edge);
			}
		}
		//hidden neurons
		for (int i=0; i<hidden; ++i){
			neuron* tmphidden_neuron = hidden_layer[i];
			for (int j=0; j<output; ++j){
				neuron* tmpoutput_neuron = output_layer[j];
				edge* tmp_edge = new edge();
				tmp_edge->weight = 0.05;
				tmp_edge->source = tmphidden_neuron;
				tmp_edge->dest = tmpoutput_neuron;
				tmphidden_neuron->outgoing.push_back(tmp_edge);
				tmpoutput_neuron->incoming.push_back(tmp_edge);
			}
		}
	}

	network::~network(){
		int num_input = input_layer.size();
		int num_hidden = hidden_layer.size();
		int num_output = output_layer.size();

		for (int i=0; i < std::max(num_input, std::max(num_hidden, num_output)); ++i){
			if (i < num_input){
				delete input_layer[i];
			}
			if (i < num_hidden){
				delete hidden_layer[i];
			}
			if (i < num_output){
				delete output_layer[i];
			}
		}
	}

	void network::teach(int num_inputs, std::vector<float> inputs){
		for (int i=0; i<input_layer.size(); ++i){
			input_layer[i]->output = inputs[i];
		}
		really_teach(hidden_layer.size(), hidden_layer);
		really_teach(output_layer.size(), output_layer);
	}

	void network::really_teach(int num_inputs, std::vector<neuron*>& neurons){
		int num_neurons = neurons.size();
		//processing neurons in a layer
		for (int i=0; i<num_neurons; ++i){
			//incoming weights for each neuron
			std::vector<float> weights;
			std::vector<float> inputs;
			std::vector<edge*>* vec = &(neurons[i]->incoming);
			for (int j=0; j<vec->size(); ++j){
				weights.push_back((*vec)[j]->weight);
				inputs.push_back((*vec)[j]->source->output);
			}
			neurons[i]->output = neurons[i]->sigmoid(neurons[i]->sum(inputs, weights));
		}	
	}

	void network::backpropagate(float expected_output){
		/*
		Calculating the errors
		*/
		
		//output neuron error
		output_layer[0]->error = (output_layer[0]->output)*(1-(output_layer[0]->output))*(expected_output-(output_layer[0]->output));
		//std::cout <<  output_layer[0]->error <<std::endl;
		//calculating error for each hidden neuron
		int num_hidden = hidden_layer.size();
		for (int i=0; i<num_hidden; ++i){
			neuron* tmphidden_neuron = hidden_layer[i];
			int current_sum = 0;
			for (int j=0; j<tmphidden_neuron->outgoing.size(); ++j){
				edge* current_edge = tmphidden_neuron->outgoing[j];
				current_sum+=(current_edge->weight)*(current_edge->dest->error);
			}
			tmphidden_neuron->error = (tmphidden_neuron->output)*(1-tmphidden_neuron->output)*current_sum;
		}

		int num_input = input_layer.size();
		
		//updating weights
		for (int i=0; i<num_input; ++i){
			neuron* tmpinput_neuron = input_layer[i];
			for (int j=0; j<tmpinput_neuron->outgoing.size(); ++j){
				edge* current_edge = tmpinput_neuron->outgoing[j];
				current_edge->weight += learning_rate*(current_edge->dest->error)*(tmpinput_neuron->output);
			}	
		}
		for (int i=0; i<num_hidden; ++i){
			edge* current_edge = hidden_layer[i]->outgoing[0];
			current_edge->weight += learning_rate*(current_edge->dest->error)*(current_edge->source->output);
		}
	}

	void network::set_learning_rate(float rate){
		learning_rate = rate;
	}

	void network::learn(std::vector<float> values, float output){
		teach(values.size(), values);
		backpropagate(output);
	}
	float network::xor3(std::vector<float> inputs){
		if (inputs.size() != 3){
			std::cout <<"Invalid number of args. 3 needed."<< std::endl;
			throw 1;
		}
		teach(inputs.size(), inputs);
		return output_layer[0]->output;
	}
}
