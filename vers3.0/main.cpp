/*
This file is subject to the terms and conditions defined in
file 'LICENSE', which is part of this source code package.

@author Zola 'adeebnqo' Mahlaza
@version 3.0
@date 22 August 2013

@quote
	A: "I'm a ninja"
	B: "No, you're not"
	A: "did you see me do that?"
	B: "do what?"
	A: "exactly"
*/
#include<iostream>
#include<fstream>
#include "network.hpp"
#include<vector>
int main(){
	typedef std::vector<float> _fvector;
	using namespace mhlzol004;
	network* xnet = new network(3,3,1);
	
	float inputs[8][3];
	float outputs[8];
	std::fstream file("test.txt");
	int line_num = 0;
	while(!file.eof()){
		char buf[8];
		file.read(buf, 8);
		int output_val = buf[6] - '0';
		outputs[line_num] = output_val;
			//reading the inputs
			for (int i=0; i<3; ++i){
				inputs[line_num][i] = buf[i*2]-'0';
			}
		++line_num;
		if (line_num == 8){
			break;
		}
	}
	//teach system for 99 iterations
	for (int i=0; i<99; ++i){
		for (int j=0; j<7; ++j){
			std::vector<float> input;
			for (int k=0; k<3; ++k){
				input.push_back(inputs[j][k]);
			}
			xnet->learn(input, outputs[j]);
		}
	}
	//testing system with the training data
	for (int j=0; j<7; ++j){
		std::vector<float> input;
		for (int k=0; k<3; ++k){
			input.push_back(inputs[j][k]);
		}
		std::cout << "expected:"<< outputs[j]<< ", result: "<<xnet->xor3(input)<< std::endl;
	}
	delete xnet;
	return 0;
}
