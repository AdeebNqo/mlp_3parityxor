/*

This file is subject to the terms and conditions defined in
file 'LICENSE', which is part of this source code package.

@author Zola 'adeebnqo' Mahlaza
@version 3.0
@date 20 August 2013

@quote 
	“I want my kids to have the things in life that I never had when I was growing up. Things like beards and chest hair.”
	-Jarod Kintz

This file contains all utility structs, types, etc for the MLP network

*/
#ifndef _util
#define _util
#include "neuron.hpp"
namespace mhlzol004{
	class neuron;
	struct edge{
		neuron* source;
		neuron* dest;
		float weight;
	};
}
#endif
